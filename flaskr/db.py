#!/usr/bin/python

import sqlite3
from flask import g

DATABASE = '/tmp/flaskr.db'


def connect_to_database():
    return sqlite3.connect(app.config['DATABASE'])
    
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_to_database()
    return db
    
def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()
        
def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    for row in rv:
      print(row)
    return (rv[0] if rv else None) if one else rv
    
    
def show_entries():
    name = query_db('select CompanyName from SiteDetails order by id desc',"",one=True)
    #cur = g.db.execute('select CompanyName from SiteDetails order by id desc')
    #entries = [dict(companyName=row[0]) for row in cur.fetchall()]
    #return render_template('index.html', name=name)
    return name
    
    
    
print(show_entries())