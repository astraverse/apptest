#!/usr/bin/python

# all the imports
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from contextlib import closing


# configuration
DATABASE = '/tmp/flaskr.db'
DEBUG = True
SECRET_KEY = 'development key'


# create our little application :)
app = Flask(__name__)
app.config.from_object(__name__)


def connect_to_database():
    return sqlite3.connect(app.config['DATABASE'])
    
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_to_database()
    return db
    
def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()
        
def query_db(query, args=(), one=False):
    dicList = []
    cur = get_db().execute(query, args)
    #rv = cur.fextchall()
    for row in cur.fetchall():
        dicList.append(make_dicts(cur,row))
    cur.close()
    if one:
        return (dicList[0])
    return (dicList)
    #return (make_dicts(cur,rv[0]) if rv else None) if one else make_dicts(cur,rv)
        
def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))
#@app.before_request
#def before_request():
#    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
        
@app.route('/')
def show_entries():
    name = query_db('select CompanyName from SiteDetails order by id desc',"",one=True)
    #cur = g.db.execute('select CompanyName from SiteDetails order by id desc')
    #entries = [dict(companyName=row[0]) for row in cur.fetchall()]
    return render_template('index.html', name=name)
        
        

if __name__ == '__main__':
    app.run()