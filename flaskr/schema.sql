DROP TABLE IF EXISTS SiteDetails;
CREATE TABLE SiteDetails (
  id integer primary key autoincrement,
  CompanyName text not null
);

insert into SiteDetails (CompanyName) values ("JJ Autos");